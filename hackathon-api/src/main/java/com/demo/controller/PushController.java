package com.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.push.request.Notification;
import com.demo.push.request.PushNotificationMessage;
import com.demo.push.service.PushNotificationService;
import com.demo.web.model.SendPushRequest;

public class PushController {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	PushNotificationService pushNotificationService;

	@RequestMapping(value = "/push", method = RequestMethod.POST)
	public ResponseEntity<?> createFbUser(@RequestBody SendPushRequest request)  {
		
		Notification notification = new Notification("unicredit hackathon", "hello alaattin");
		
		PushNotificationMessage message = new PushNotificationMessage.MessageBuilder()
			    .toToken(request.getDeviceToken()) // single android/ios device
			    .notification(notification)
			    .addData("firstName","okan")
			    .addData("lastName", "oktar")
			    .build();
		
		pushNotificationService.sendPush(message);
		
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

}
