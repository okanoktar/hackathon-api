package com.demo.push.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.demo.push.request.PushNotificationMessage;
import com.demo.util.JsonUtils;

@Service
public class PushNotificationServiceImpl implements PushNotificationService {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private PushNotificationClientSettings settings;
	
	@Autowired
	public PushNotificationServiceImpl(PushNotificationClientSettings settings) {
		this.settings = settings;
	}
	
	@Override
	public void sendPush(PushNotificationMessage message) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();
		
		httpHeaders.set("Authorization", "key=" + settings.getApiKey());
		httpHeaders.set("Content-Type", "application/json");
		
		String json = JsonUtils.getAsJsonString(message);

		HttpEntity<String> httpEntity = new HttpEntity<String>(json, httpHeaders);
		String response = restTemplate.postForObject(settings.getApiUrl(), httpEntity, String.class);
		logger.info("SEND PUSH RESPONSE : " + response);
	}
	
	
	public void sendPushWithRetry(PushNotificationMessage message) {
		SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy();
	    retryPolicy.setMaxAttempts(5);
	 
	    ExponentialBackOffPolicy backOffPolicy = new ExponentialBackOffPolicy();
	    backOffPolicy.setInitialInterval(500);
	    backOffPolicy.setMultiplier(2.0);
	 
	    RetryTemplate template = new RetryTemplate();
	    template.setRetryPolicy(retryPolicy);
	    template.setBackOffPolicy(backOffPolicy);
	    
	    template.execute(new RetryCallback<Void, ResourceAccessException>() {
	      @Override
	      public Void doWithRetry(RetryContext retryContext) throws ResourceAccessException {
	        logger.debug(">RetryCount: {}", retryContext.getRetryCount());
	        //sendPushNoRetry(message);
	        return null;
	      }
	    });
	  }
	
	

}
